## Summary
(Enter a summary of the New Feature here.)

## Details
(Enter any details, clarifications, answers to questions, or points about implementation here.)

## Additional Information
(Enter any background or references such as Stack Overflow, MSDN, blogs, etc. that may help with developing the feature.)

/label ~feature-proposal
