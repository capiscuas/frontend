## What does this MR do?

<!-- Briefly describe what this MR is about -->

## Related issues

<!-- Mention the issue(s) this MR closes or is related to -->

Closes

## Unit Testing

(Which unit tests did you made? Specify them by pasting the output or make a checklist describing them.)

## Integration tests

(Do this PR needs integration tests? If so, please specify which integration tests you made with a list or an execution.)

## Expand and contract

(Do this MR needs expand and contract? or is part of one? If so, please specify which expand and contract is this part of.)

## Related MRs

(Does this MR have some type of relationship with another MR in one of IZ repositories? If so specify the type of relationship and the other MRs involved. If those MRs are still not done please specify as such.)

# Checklist of things to review

- [ ] Enough and correct logging.
- [ ] Is the documentation up to date with this changes?
- [ ] Does the migration think of all the cases it can find?
- [ ] Does the migration has the correct precondition?
- [ ] Does the migration has a good down step?
- [ ] Naming of things.
  - [ ] Functions/methods/variables
- [ ] Prioritize native libraries over 3rth party ones.
- [ ] If this MR is apart of a expand phase, Does this will collision with the current state of the DB, endpoints, events? it has the correct deprecation comments notices?

/label ~code
