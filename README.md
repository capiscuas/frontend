# Pam a Pam

Pam a Pam és una eina col·lectiva que mostra que existeix una economia al servei de les persones a Catalunya i et convida a participar-hi. Pots fer-ho com a usuària, buscant en el mapa aquelles iniciatives d’alimentació, habitatge, serveis, energia… que et permetin consumir d’acord amb els teus valors. Pots ser membre de la comunitat oberta que detecta i entrevista els punts que apareixen en el mapa. I pots sumar-t’hi com a iniciativa, visibilitzar-te al mapa, i articular-te amb d’altres experiències d’economia solidària.

Som una comunitat oberta de persones que ens formem en economia solidària, entrevistem les iniciatives i les pugem al mapa per a que tu les coneguis. Alhora, aquest procés facilita l’enxarxament entre iniciatives, així com obtenir diagnòstics de les diferents pràctiques transformadores que existeixen a Catalunya.

Som un espai d’aprenentatge col·lectiu a través de l’activisme que facilita practicar el consum responsable i enfortir l’economia solidària.

![ESS](/uploads/d0f0d9d90a0c21db8e9fd29f8448ffa7/organizacio-pamapam.jpg)


## Frontend

Aquest és un dels projectes de Pam a Pam.

El repositori frontend de Pam a Pam serveix per millorar el web [https://pamapam.org/](https://pamapam.org/) que està realitzada amb Wordpress.


## Com instal·lar-lo?

https://gitlab.com/pamapam/frontend-dev


## Document

Pots trobar documentació relativa a aquest repositori a:

[Documentació](https://gitlab.com/pamapam/frontend/wikis/home)


## Participa!

Benvinguda al projecte de la comunitat digital de Pam a Pam!

El projecte s'està realitzant amb una comunitat de programadores, majoritàriament dones, que treballem col·laborativament.

Si vols contribuir a millorar el codi llegeix abans:[CONTRIBUTING.md](https://gitlab.com/pamapam/frontend/blob/master/CONTRIBUTING.md)