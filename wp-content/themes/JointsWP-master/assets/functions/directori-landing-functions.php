<?php

function entities_grid() {
	global $baseApiInternalUrl;
	global $baseApiUrl;
		
	$page = empty($_POST['page']) ? 0 : $_POST['page'];
	$size = empty($_POST['size']) ? 12 : $_POST['size'];
	if ($size != 12){
		$old_size = 12;
		$old_page = ((int) $size / $old_size) - 1;
	} else {
		$old_size = $size;
		$old_page = $page;
	}
	$territoryId = $_POST['territoryId'];
	$territoryType = $_POST['territoryType'];
	$text = empty($_POST['text']) ? "" : $_POST['text'];
	$sectorIds = empty($_POST['sectorIds']) ? [] : $_POST['sectorIds'];

	$data = array(
		'page' => $page,
		'size' => $size,
		'territoryId' => $territoryId,
		'territoryType' => $territoryType,
		'text' => $text,
		'sectorIds' => $sectorIds
	);
	$jsonData = json_encode($data);
	$postArray = array(
		'headers' => array('Content-Type' => 'application/json'),
		'body' => $jsonData
	);
	$postUrl = $baseApiInternalUrl . "/searchEntities";

	$entities_response = wp_remote_post($postUrl, $postArray);
	$entities_array = json_decode(wp_remote_retrieve_body($entities_response));

	foreach ($entities_array->response->content as $entity) {
		include 'entity-box.php';
	}

	if (!$entities_array->response->lastPage) {
		echo '<div id="entities-next-page">';
		echo '	<button id="more-contents" type="button" class="control control-text button" onclick="moreContents(' . strval((int) $old_page + 1) . ', ' . $old_size . ')">';
		echo '		VEURE MÉS';
		echo '	</button>';
		echo '</div>';
	}

	if (wp_doing_ajax()) {
		die();
	}
	
}

function provinces_options() {
	global $baseApiInternalUrl;

	$provinces_request = $baseApiInternalUrl . "/provinces";
	$provinces_response = wp_remote_get($provinces_request);
	$provinces_array = json_decode(wp_remote_retrieve_body($provinces_response));

	echo '<option class="control" data-filter="all" value="defaultValue">Tots els territoris</option>';
	foreach ($provinces_array as $province) {
		echo '<option class="control" data-filter="' . strval($province->id) . '" value="' . strval($province->id) . '">' . $province->name . '</option>';
	}
}

function main_sectors_options() {
	global $baseApiInternalUrl;

	$sectors_request = $baseApiInternalUrl . "/mainSectors";
	$sectors_response = wp_remote_get($sectors_request);
	$sectors_array = json_decode(wp_remote_retrieve_body($sectors_response));

	echo '<option class="control" data-filter="all" value="defaultValue">Tots el sectors</option>';
	foreach ($sectors_array as $sector) {
		echo '<option class="control" data-filter="' . strval($sector->id) . '" value="' . strval($sector->id) . '">' . $sector->name . '</option>';
	}
}
