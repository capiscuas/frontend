<?php

function call_entity(){
	global $baseApiInternalUrl;
	global $baseApiUrl;

	global $entity_id;

	$entity_request = $baseApiInternalUrl . "/entities/" . $entity_id;
	$entity_response = wp_remote_get($entity_request);
	$entity = json_decode(wp_remote_retrieve_body($entity_response))->response;
	return $entity;

}
function call_criteria(){
	global $baseApiInternalUrl;
	global $baseApiUrl;

	global $entity;

	$criteria_request = null;
	if ($entity->oldVersion) {
		$criteria_request = $baseApiInternalUrl . "/oldCriteria";
	} else {
		$criteria_request = $baseApiInternalUrl . "/criteria";
	}

	$criteria_response = wp_remote_get($criteria_request);
	$criteria = json_decode(wp_remote_retrieve_body($criteria_response))->response;

	return $criteria;
}
function entity_single() {
	global $baseApiInternalUrl;
	global $baseApiUrl;
	global $entity;
	global $criteria;

	include 'entity-details.php';

	if (wp_doing_ajax()) {
		die();
	}
}

function related_entities_sector($entity_id) {
	global $baseApiInternalUrl;
	global $baseApiUrl;

	$entity_request = $baseApiInternalUrl . "/relatedEntitiesBySector/" . $entity_id . '/3';
	$entity_response = wp_remote_get($entity_request);
	$related_entities = json_decode(wp_remote_retrieve_body($entity_response))->response;

	foreach ($related_entities as $entity) {
		include 'entity-box.php';
	}
}

function related_entities_proximity($entity_id) {
	global $baseApiInternalUrl;
	global $baseApiUrl;

	$entity_request = $baseApiInternalUrl . "/relatedEntitiesByProximity/" . $entity_id . '/3';
	$entity_response = wp_remote_get($entity_request);
	$related_entities = json_decode(wp_remote_retrieve_body($entity_response))->response;

	foreach ($related_entities as $entity) {
		include 'entity-box.php';
	}
}
