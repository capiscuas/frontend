<?php
$groupedSectors = [];
foreach ($entity->sectors as $sector) {
	$sectorNames = $groupedSectors[$sector->iconUrl];
	if ($sectorNames == null) {
		$sectorNames = $sector->name;
		$groupedSectors[$sector->iconUrl] = $sectorNames;
	} else {
		$sectorNames .= ', ' . $sector->name;
	}
}
$mainDivClass = 'mix ' . $sectorsString . ' ' . $entity->province->name;
?>

<div class="<?php echo $mainDivClass; ?>" data-search="<?php echo $entity->name . ' ' . $entity->description; ?>">
	<div class="large-4 medium-4 columns panel" data-equalizer-watch>
		<div class="favorite">
			<span class="icon-xinxeta">
		</div>
		<article id="post-<?php echo $entity->normalizedName; ?>" class="<?php echo join(' ', get_post_class('')); ?>" role="article">
			<div class="entity-box-header">
				<section class="featured-image" itemprop= "articleBody" style="background-image: url(<?php echo $baseApiUrl . $entity->pictureUrl; ?>">
					<a onclick="replaceLocation('<?php echo $old_page; ?>', '<?php echo $entity->normalizedName; ?>');" href="/directori/<?php echo $entity->normalizedName; ?>">
						<img src="<?php echo $baseApiUrl . $entity->pictureUrl; ?>"/>
					</a>
				</section>
				<header class="article-header">
					<h3 class="title">
						<a onclick="replaceLocation('<?php echo $old_page; ?>', '<?php echo $entity->normalizedName; ?>');" href="/directori/<?php echo $entity->normalizedName; ?>"><?php echo $entity->name; ?></a>
					</h3>
				</header>
				<section class="entry-content" itemprop="articleBody">
					<p><?php echo $entity->province->name; ?></p>
				</section>
				<section class="sectors" itemprop="articleBody">
<?php
					foreach ($groupedSectors as $iconUrl => $sectorNames) {
?>
						<img src="<?php echo $baseApiUrl . $iconUrl; ?>" width="24" height="24" alt="<?php echo $sectorNames; ?>" />
<?php
					}
?>
				</section>
			</div>
			<section class="entry-content description ellipsis" itemprop="articleBody">
				<p><?php echo $entity->description; ?></p>
			</section>
			<section class="veurefitxa semi-bold" itemprop="articleBody">
				<a onclick="replaceLocation('<?php echo $old_page; ?>', '<?php echo $entity->normalizedName; ?>');" href="/directori/<?php echo $entity->normalizedName; ?>"><span class="icon-arrow-right"></span><p>Veure més</p></a>
			</section>
		</article>
	</div>
</div>
