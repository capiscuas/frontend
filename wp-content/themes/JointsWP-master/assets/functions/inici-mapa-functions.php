<?php

function entities_map() {
	global $baseApiInternalUrl;
	global $baseApiUrl;

	//$page = empty($_POST['page']) ? 0 : $_POST['page'];
	//$size = empty($_POST['size']) ? 12 : $_POST['size'];

	//$entities_request = $baseApiInternalUrl . "/entities?page=" . $page . "&size=" . $size;
	$entities_request = $baseApiInternalUrl . "/entities";
	$entities_response = wp_remote_get($entities_request);
	//$entities_array = json_decode(wp_remote_retrieve_body($entities_response));
	$entities_array =json_encode(wp_remote_retrieve_body($entities_response));



	if (wp_doing_ajax()) {
		die();
	}

	return $entities_array;
}
?>
