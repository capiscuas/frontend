<article role="article" class="bloc-xinxeta-article">
	<div class="bloc-xinxeta">
		<div class="celito left green"></div>
		<div class="celito right orange"></div>
		<div class="large-4 medium-4 small-12 columns comunitat">
			<div style="text-align: center">
				<img class="<?php echo $classImageUrl ?>" src="<?php echo $userImageUrl ?>" >
			</div>
			<header class="article-header">
				<h1 class="entry-title single-title"><?php echo ($pamapamUser->name . ' ' . $pamapamUser->surname); ?></h1>
			</header>
		</div>
		<div id="main" class="large-5 medium-4 small-12 columns single-xinxeta" role="main">
		<p class="descripcio"><?php echo $pamapamUser->description; ?></p>
<!--
		<p>Territori: aquí el territori</p>
		<p>Membre de Pamapam des de fa:<br/>aquí la data d\'entrada</p>
-->
		<hr>
			<p>Xarxes:</p>
<?php
			if ($pamapamUser->facebook) {
?>
				<span class="fn"><a href="<?php echo $pamapamUser->facebook; ?>" target="_blank"><span class="icon-facebook"></span></a></span>
<?php
			}
			if ($pamapamUser->twitter) {
?>
				<span class="twitter"><a href="<?php echo $pamapamUser->twitter; ?>" target="_blank"><span class="icon-twitter"></span></a></span>
<?php
			}
?>
			</div>
			<div class="large-3 medium-4 small-12 columns favorite">
			</div>
		</hr>
	</div>
</article>