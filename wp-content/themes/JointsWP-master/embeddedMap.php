<!doctype html>
<?php 
$base_dir = dirname(dirname(__FILE__));
require_once($base_dir."../../../wp-load.php");
?>
<html>
 
<head>
	<?php wp_head(); ?>
	
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"
		integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
		crossorigin=""></link>
	<script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"
		integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
		crossorigin=""></script>
	
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/sumoselect.css">
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/jquery.sumoselect.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dotdotdot/jquery.dotdotdot.min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.1.0/dist/MarkerCluster.Default.css">
	<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.1.0/dist/MarkerCluster.css">
	<script src="https://unpkg.com/leaflet.markercluster@1.1.0/dist/leaflet.markercluster.js"></script>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/js/leaflet-sidebar-master/L.Control.Sidebar.css" />
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/leaflet-sidebar-master/L.Control.Sidebar.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/map.js"></script>
</head>

<?php
include "map-head.php"
?>

<body>
<?php
include "map-body.php"
?>
