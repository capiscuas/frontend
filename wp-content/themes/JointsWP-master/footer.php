				<footer class="footer" role="contentinfo">
					<div id="inner-footer" class="row">
						<div class="large-12 medium-12 columns">
							<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pamapam-logo-footer.png" alt="Pamapam" /></a>
							</div>
							</div> <!-- end #inner-footer -->





				</footer> <!-- end .footer -->



			<div class="cont-footer-2 large-12 medium-12 small-12 columns">
				<div class="footer-2 large-6 medium-6 small-6 columns contacta-footer">

						<strong><a href="/avis-legal/">Avís legal</a> - <a href="/politica-de-cookies/">Política de cookies</a> - <a href="/politica-de-privacitat/">Política de privacitat</a> - <a href="/contacta">Contacta</a></strong>

		</div> <!--end footer-2-->


		<div class="footer-2 large-6 medium-6 small-6 columns copyright-footer">

		<p class="source-org creativecommons"><?php echo date('Y'); ?> <?php bloginfo('name'); ?>&nbsp; <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a></p>

</div> <!--end footer-2-->

</div> <!--contenido footer-2-->



			</div>  <!-- end .main-content -->
		</div> <!-- end .off-canvas-wrapper -->


		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->
