<?php /* Template Name: Inici No Sidebar */ ?>

<?php get_header(); ?>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" /> 

<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
crossorigin=""></script> 


<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/sumoselect.css">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/jquery.sumoselect.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.1.0/dist/MarkerCluster.Default.css">
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.1.0/dist/MarkerCluster.css">
<script src="https://unpkg.com/leaflet.markercluster@1.1.0/dist/leaflet.markercluster.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/js/leaflet-sidebar-master/L.Control.Sidebar.css" />
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/leaflet-sidebar-master/L.Control.Sidebar.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/home.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/map.js"></script>

<?php
$mapEntityStatuses = [ "PUBLISHED" ];
include "map-head.php";
?>

<div id="content">

<?php
	include "map-body.php";
?>

	<div id="inner-content" class="row">
		<main id="main" class="large-12 medium-12 columns" role="main">
			<!-- begin page loop -->
			<div class="row">
<?php
				$my_query = new WP_Query('page_id=33');
				while ($my_query->have_posts()) :
					$my_query->the_post();
					$do_not_duplicate = $post->ID;
?>

				<!-- geolocalitzacio -->
				<div class="large-12 small-12 columns geo">
					<div class="large-6 medium-6 small-12 columns">
<?php
						if (get_field('vols_saber')) {
							echo '<p>' . get_field('vols_saber') . '</p>';
						}
?>
					</div>
				</div>
				<!-- end geolocalitzacio -->
				<!-- box 01 -->
				<div class="large-12 medium-12 columns box-01">
					<div class="celito left purple"></div>
					<div class="celito right orange"></div>
					<div class="large-5 columns">
<?php
						if (get_field('text_narratiu_directori_de_punts')) {
							echo '<p>' . get_field('text_narratiu_directori_de_punts') . '</p>';
						}
?>
					</div>
					<div class="large-7 columns">
						<h2 class="rubik">
							<a href="<?php the_field('link_directori'); ?>">
<?php
								if (get_field('titol_franja_directori_punts')) {
									echo get_field('titol_franja_directori_punts');
								}
?>
							</a>
						</h2>
						<a href="<?php the_field('link_directori'); ?>"><span class="icon-arrow-right"></span></a>
					</div>
				</div>
				<!-- box 02 -->
				<div class="large-12 medium-12 columns box-02">
					<div class="large-6 columns">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/bqH23jCHssc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="large-6 columns">
						<h3 class="rubik">
<?php
							if (get_field('zona_01_texto_1')) {
								echo get_field('zona_01_texto_1');
							}
?>
						</h3>
						<p>
<?php
							$content = get_the_content();
							print $content;
?>
						</p>
					</div>
				</div>
				<!-- box 03 -->
				<div class="large-12 medium-12 columns box-03">
					<div class="celito left orange"></div>
					<div class="celito right green"></div>
					<div class="large-5 columns">
<?php
						if (get_field('text_narratiu_criteris')) {
							echo '<p>' . get_field('text_narratiu_criteris') . '</p>';
						}
?>
					</div>
					<div class="large-7 columns">
						<h2 class="rubik">
							<a href="<?php echo get_home_url();?>/criteris/">
<?php
								if (get_field('boto_franja_criteris')) {
									echo get_field('boto_franja_criteris');
								}
?>
							</a>
						</h2>
						<a href="<?php echo get_site_url();?>/criteris/"><span class="icon-arrow-right"></span></a>
					</div>
				</div>
			</div>
			<!-- END page loop -->
				<?php endwhile; ?>
		</main>
		<!-- end #main -->
	</div>
	<!-- end #inner-content -->
	<!-- blog -->
	<div class="contain-to-grid blog">
		<div class="large-6 columns comunitat banner">
			<div><?php
				$text_banner_01 = get_field('text_banner_01');
				if($text_banner_01)
				{
					$url = get_home_url() . "/comunitat/";
					echo "<a href='${url}'> ${text_banner_01}</a>";
				}
			?></div>
			<a href="<?php echo get_site_url();?>/comunitat/">
				<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/09/11_banner_comunitat_quadrat-petit.jpg">
			</a>
		</div>
		<div class="large-6 columns xinxeta banner">
			<div><?php
				$text_banner_02b = get_field('text_banner_02b');
				if($text_banner_02b)
				{
					$url = get_home_url() . "/xinxeta/";
					echo "<a href='${url}'> ${text_banner_02b}</a>";
				}
			?></div>
			<a href="<?php echo get_site_url();?>/xinxeta/">
				<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/09/feste-xinxeta-banner.jpg">
			</a>
		</div>
		<div class="row">
			<div class="large-8 columns blog">
				<div class="large-12 columns">
					<h2 class="rubik">Blog</h2>
				</div>
                <?php query_posts($queryPost);?>

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="large-6 columns">
                    <?php get_template_part( 'parts/loop', 'archive' ); ?>
                </div>
                <?php endwhile; ?>
                <?php endif; wp_reset_query(); ?>

            </div>
			<div class="large-4 columns">
				<div class="large-12 columns">
					<h2 class="rubik">Xarxes</h2>
				</div>
				<a class="twitter-timeline" data-height="850" href="https://twitter.com/pamapamcat"></a>
				<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
			</div>
		</div>
	</div>
	<!-- end blog -->
	<div class="contain-to-grid sand-bkg">
		<?php get_template_part( 'parts/include', 'afiliation' ); ?>
	</div>
</div>

<?php get_footer(); ?>
