function updateDotdotdot() {
	$('.ellipsis').dotdotdot({
		ellipsis : '...',
		wrap : 'word'
	});
}

jQuery(document).ready(function() {

	updateDotdotdot();

});
