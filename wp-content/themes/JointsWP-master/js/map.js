function updateDotdotdot() {
	jQuery(".entity-slider-box-description").dotdotdot({
		ellipsis : '...',
		wrap : 'word',
		height : 90,
		fallbackToLetter : true,
		watch : true
	});
 
	jQuery(".web").dotdotdot({
		ellipsis : '...',
		wrap : 'word',
		height : 30,
		fallbackToLetter : true,
		watch : true
	});

	if (!jQuery(".web").hasClass("is-truncated")) {
		jQuery(".web a").removeAttr("title");
	}

	jQuery(".adress").dotdotdot({
		ellipsis : '...',
		wrap : 'word',
		height : 20,
		fallbackToLetter : true,
		watch : true
	});

	if (!jQuery(".adress").hasClass("is-truncated")) {
		jQuery(".adress").removeAttr("title");
	}

}


function getLeafletIcon(iconUrl) {
	var leafletIcon = null;
	if (iconUrl == null) {
		leafletIcon = noSectorIcon;
	} else {
		leafletIcon = leafletIcons[iconUrl];
		if (leafletIcon == null) {
			leafletIcon = new L.icon({
            	iconUrl: iconUrl,
            	iconSize: [34, 47],
            	iconAnchor: [17, 46],
				popupAnchor: [0, -40]
        	});
        	leafletIcons[iconUrl] = leafletIcon;
		}
	}

	return leafletIcon;
}

function getSidebarContents(feature) {
	console.log(feature);
	var criteriaIconsDiv = '<div class="entity-slider-box-icons">';
	feature.properties.criteriaIconUrls.forEach(function (iconUrl) {
		criteriaIconsDiv += '<img src=\"' + jsBaseResourceUrl + iconUrl + '\"/>'
	});
	criteriaIconsDiv += '</div>'

	var sector = feature.properties.sector;
	var sectores = sector.split(" - ");

	if (sectores.length == 2) {
		sector = sectores[1]; // Si tiene subsector cogerlo
	}

	var articleUrl = '/directori/' + feature.properties.normalizedName;
	var visitaperf = '';
    if (feature.properties.status === 'EXTERNAL'){
    	visitaperf = '<p> Aquesta iniciativa no esta a Pam a Pam <p>';
	} else {
        visitaperf = '<a href="' + articleUrl + '" target="_blank" >' +
            '<button id="view-details" type="button" class="control control-text button">VISITAR PERFIL</button>' +
            '</a>';
	}
	var contents =
		'<div id="punts" class="punts ficha-punts">' +
		'<h3>' +
		'<a class="titular-ficha" href="' + articleUrl + '" target="_blank">' +
			(feature.properties.name ? feature.properties.name : '') +
		'</a>' +
		'</h3>' +
			'<div class="entity-slider-box-header">' +
				'<a href="' + articleUrl + '" target="_blank">' +
					'<img src="' + jsBaseResourceUrl + feature.properties.pictureUrl + '" >' +
				'</a>' +
				'<div class="entity-slider-box-info">' +

					'<span class="adress" title="' + feature.properties.address +'">' + '<i class="fa fa-map-marker" aria-hidden="true"></i>' +
						(feature.properties.address ? feature.properties.address : '') +
					'</span>' + '<br/>' +
					'<span>' +
						'<b>' + (feature.properties.town ? feature.properties.town : '') + '</b><br>' +
					'</span>' +
					'<span class="tel">' + '<i class="fa fa-phone" aria-hidden="true"></i>' +
            '&nbsp;&nbsp;' + (feature.properties.phone ? feature.properties.phone : '') +
          '</span>' + '<br/>' +
          '<span class="email">' + '<i class="fa fa-envelope-o" aria-hidden="true"></i>' +
          '&nbsp;&nbsp;' + (feature.properties.email ? feature.properties.email : '') +
          '</span>' + '<br/>' +
          '<span class="web">' +
            (feature.properties.web ? '<i class="fa fa-globe" aria-hidden="true"></i>' +
            '&nbsp;&nbsp;' + '<a href="' + feature.properties.web + '" target="_blank" title="' + feature.properties.web + '">' + feature.properties.web + '</a>'
            : '') +
          '</span>' +
            '<br>' +
              (feature.properties.facebook ? '<span class="fn"><a href="' +  feature.properties.facebook + '" target="_blank"><span class="icon-facebook"></span></a></span>' : '') +
              (feature.properties.twitter ? '<span class="twitter"><a href="' + feature.properties.twitter + '" target="_blank"><span class="icon-twitter"></span></a></span>' : '') +
              (feature.properties.googleplus ? '<span class="googleplus"><a href="' + feature.properties.googleplus + '" target="_blank"><span class="icon-google-plus"></span></a></span>' : '') +
              (feature.properties.instagram ? '<span class="instagram"><a href="' +  feature.properties.instagram + '" target="_blank"><span class="icon-pinterest"></span></a></span>' : '') +
              (feature.properties.pinterest ? '<span class="pinterest"><a href="' +  feature.properties.pinterest + '" target="_blank"><span class="icon-instagram"></span></a></span>' : '') +
              (feature.properties.quitter ? '<span class="quitter"><a href="' +  feature.properties.quitter + '" target="_blank"><span class="icon-quitter"></span></a></span>' : '') +
				'</div>' +
			'</div>' +
			'<div class="entity-slider-box-sectors">' +
        '<div class="entity-slider-box-icons">' +
          sector + '&nbsp;<img src=\"' + jsBaseResourceUrl + feature.properties.sectorIconUrls[0] + '\"/>' +
        '</div>' +
      '</div>' +
			'<div class="entity-slider-box-contents">' +
				'<div class="entity-slider-box-description">' +
					(feature.properties.description ? feature.properties.description : '') +
				'</div>' +
				'<div class="entity-slider-box-criteria">' +
					'<h4>Criteris</h4>' +
					criteriaIconsDiv +
				'</div>' +
				'<div class="entity-slider-box-button">' +
        			visitaperf +
				'</div>' +
			'</div>' +
		'</div>';

	return contents;
}

function refreshMap(data) {
	markers.clearLayers();
	sidebar.hide();
	map.setView(mapCenter, mapZoom, {
		"animate": true,
		"pan": {
			"duration": 3
		}
	});
	var leafletPoints = L.geoJson(data, {
		pointToLayer: function(feature, latlng) {
			var leafletIcon = this.getLeafletIcon(jsBaseResourceUrl + feature.properties.sectorMapIconUrl);
			return L.marker(latlng, {
				icon: leafletIcon
			})
			.on('mouseover', function() {
				var popup = L.popup({
						closeButton: false
					})
					.setContent(feature.properties.name);

				this.bindPopup(popup).openPopup();
			})
			.on('mouseout', function() {
				this.closePopup();
			})
			.on('click', function() {
				// se crea la info de la iniciativa, que mostrará al hacer click
				sidebar.setContent(getSidebarContents(feature));
				sidebar.show();
				updateDotdotdot();
			});
		}
	});

	markers.addLayer(leafletPoints);

}

function searchLocation(e) {
	L.marker(e.latlng).addTo(map);
}

function doSearch() {
	var url = jQuery('#searchEntitiesForm').attr("action");
	var selectedSectors = [];
    selectedSectorsMap.forEach(function (item, key, mapObj) {
        selectedSectors.push(key);
	});

    jQuery('#searchText').attr("placeholder", 'cerca una adreça, una botiga, etc');

	sendData = {
			'text' : jQuery('#searchText').val(),
			'sectorIds' : selectedSectors,
			'entityStatusTypes' : mapEntityStatuses,
			'externalFilterTags' : mapFilterTags
	};
	jQuery.ajax({
		url: url,
		type: "POST",
		data: JSON.stringify(sendData),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function(data) {
			enabledCheckBox();
			if ( data.response.length == 0 ){
				jQuery('#searchText').attr("placeholder", 'Aquesta cerca no té resultats');
				jQuery('#searchText').val("");
			} else {
				refreshMap(data.response);
            }
		}
	});
}

var noSectorIcon = new L.icon({
	iconUrl: 'icones/sm-no-sector.png',
	iconSize: [34, 47],
	iconAnchor: [17, 46],
	popupAnchor: [0, -40]
});

var leafletIcons = {};

var tiles = L.tileLayer('https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: 'Map data © <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
    opacity: 0.8
});

var markers = new L.MarkerClusterGroup({
	maxClusterRadius: 20
});

var map = null;
var sidebar = null;

var selectedSectorsMap = new Map();

jQuery(document).ready(function() {

	jQuery('.search-box-panel').show('fast');

	map = L.map('map', {
		center: mapCenter,
		zoom: mapZoom,
		zoomControl: false
	});

	map
		.addLayer(tiles) // añadimos el mapa
		.addLayer(markers) // añadimos los marcadores del ClusterGroup
		.scrollWheelZoom.disable(); // scroll disable, (enable cuando haces click)

	map.on('click', function() {
		if (map.scrollWheelZoom.enabled()) {
			map.scrollWheelZoom.disable();
		} else {
			map.scrollWheelZoom.enable();
		}
	});

	L.control.zoom({
		position: 'topright'
	}).addTo(map);

	sidebar = L.control.sidebar('sidebar', {
		closeButton: true,
		position: 'right'
	});

	jQuery('#searchText').mouseover(function(){
		map.doubleClickZoom.disable();
	});
	jQuery('#searchText').mouseout(function(){
		map.doubleClickZoom.enable();
	});
	jQuery('#searchText').mousedown(function(){
		map.dragging.disable();
	});
	jQuery(document).mouseup(function(){
		map.dragging.enable();
	});

	map.addControl(sidebar);

	jQuery('#geolocalitzacio').click(function(ev) {
		map.on('locationfound', searchLocation);
		map.locate({setView: true, maxZoom:12});
	});
	
	jQuery("#searchEntitiesForm").submit(function(event) {
		event.preventDefault();
		console.log('SearchEntities')
		doSearch();
	});

	//jQuery("#searchEntitiesForm").submit();

	jQuery("#searchText").change(function() {
		doSearch();
	});

	jQuery("#selFiltres").click(function(event) {
		event.preventDefault();
		jQuery('.listSector').toggle("slow");
	});

	jQuery(".searchSectors input").change(function() {
        disabledCheckBox();
		if(this.checked) {
			selectedSectorsMap.set(this.id,this.value);
		} else {
			selectedSectorsMap.delete(this.id);
		}
		doSearch();
	});

	updateDotdotdot();

	jQuery('.search-menu').on('click', function() {
		jQuery('#formText').toggleClass("hide-for-small-only");
		jQuery('#geolocalitzacio').toggleClass("hide-for-small-only");
	});
    jQuery('#formText').on('click', function() {
        jQuery('#searchText').focus();

    });
    doSearch();
});

function disabledCheckBox() {
	jQuery(".searchSectors input").attr("disabled", true);
	jQuery("#preload").show();
}
function enabledCheckBox() {
    jQuery(".searchSectors input").removeAttr("disabled");
    jQuery("#preload").hide();

}
jQuery(document).keyup(function (e) {
	//input controll enter
	if (jQuery("#searchText").is(":focus") && (e.keyCode == 13)) {
    	jQuery('#formText').toggleClass("hide-for-small-only");
	}
});
