function refreshTerritory(territorySelect, territories) {
	var options = territorySelect.prop('options');
	territorySelect.empty();
	options[0] = new Option("", 0);
	territories.forEach(function (region) {
		options[options.length] = new Option(region.name, region.id);
	});
}

function loadProvinces() {
	var url = jsBaseApiUrl + '/provinces';
	$.get(url, function(data) {
		refreshTerritory($('#f_provinces'), data.response.content);
	});
}

function loadRegions(provinceId) {
	if (provinceId) {
		var url = jsBaseApiUrl + '/provinces/' + provinceId;
		$('#f_towns').empty();
		$('#f_districts').empty();
		$('#f_neighborhoods').empty();
		$.get(url, function(data) {
			refreshTerritory($('#f_regions'), data.response.regions);
		});
	}
}

function loadTowns(regionId) {
	if (regionId) {
		var url = jsBaseApiUrl + '/regions/' + regionId;
		$('#f_districts').empty();
		$('#f_neighborhoods').empty();
		$.get(url, function(data) {
			refreshTerritory($('#f_towns'), data.response.towns);
		});
	}
}

function loadDistricts(townId) {
	if (townId) {
		var url = jsBaseApiUrl + '/towns/' + townId;
		$('#f_neighborhoods').empty();
		$.get(url, function(data) {
			refreshTerritory($('#f_districts'), data.response.districts);
		});
	}
}

function loadNeighborhoods(districtId) {
	if (districtId) {
		var url = jsBaseApiUrl + '/districts/' + districtId;
		$.get(url, function(data) {
			refreshTerritory($('#f_neighborhoods'), data.response.neighborhoods);
		});
	}
}

jQuery(document).ready(function() {
	loadProvinces();

    $("#f_provinces").change(function() {
    	loadRegions(this.value);
    });

    $("#f_regions").change(function() {
    	loadTowns(this.value);
    });

    $("#f_towns").change(function() {
    	loadDistricts(this.value);
    });

    $("#f_districts").change(function() {
    	loadNeighborhoods(this.value);
    });

});
