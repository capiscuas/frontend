<?php
$iframe = false;

if (!empty($_SERVER['HTTP_REFERER'])) {
    $iframe = true;
    $referer = explode("://", $_SERVER['HTTP_REFERER']); //separo http
    $referer = explode(".", $referer[1]); //separo por punto qa.pamapam.org
    foreach ($referer as $ref) {
        if ($ref === 'pamapam') { //si contiene pamapam no es un iframe
            $iframe = false;
        }
    }
};

?>
<!-- inicio mapa -->
<!-- no cambiar el id. Requiere el plugin leaflet-sidebar-master -->
<div id="sidebar">
    <p></p>
</div>
<!-- inicio lista filtros. Requiere mapbox. No cambiar id -->
<!-- fin lista filtros -->
<!-- overlay para el movil-->
<div class="overlay " onClick="style.pointerEvents='none'"></div>

<div id="map" class="contain-to-grid">
    <div id="geolocalitzacio" class="buttons-on-map geolocalitzacio">
        <a href="#" class="button"><span class="icon-ic_room_48px"></span>GEOLOCALITZA'M</a>
    </div>
    <?php if ($iframe): ?>
        <div id="add-iniciativa" class="buttons-on-map add-iniciativa">
			<a href="https://pamapam.org/"  target="_blank" class="">
				<img  src="<?php echo get_template_directory_uri() . "/assets/images/pamapam-logo-bn.png"; ?>" style="width: 150px;">
			</a>
		</div>
    <?php endif; ?>
    <?php if (!$iframe): ?>
        <div id="add-iniciativa" class="buttons-on-map add-iniciativa">
            <a href="<?php echo get_site_url() . "/afegeix-una-iniciativa/"; ?>" class="button">
                <span class="icon-xinxeta"></span>AFEGEIX UNA INICIATIVA <span class="icon-arrow-right"></span>
            </a>
        </div>
    <?php endif; ?>
    <div>
            <span class="search-menu show-for-small-only">
                <img src="<?php echo get_template_directory_uri() . "/assets/images/fi-magnifying-glass-white.svg"; ?>">
            </span>
    </div>
    <div class="search-box-panel">
        <form method="post" id="searchEntitiesForm" action="<?php echo $baseApiUrl . "/searchEntitiesGeojson"; ?>">
        </form>
        <div id="formText" class="small-12 background-transparent hide-for-small-only">
            <input id="searchText" class="searchText" type="text" name="text"
                   placeholder="cerca una adreça, una botiga, etc" value="" autofocus/>
            <span class="search-home hide-for-small-only">
					<img src="<?php echo get_template_directory_uri() . "/assets/images/fi-magnifying-glass.svg"; ?>">
			</span>
            <div class="filter">
                <button id="selFiltres" style="float: left;color:lightgrey">
                    <img src="<?php echo get_template_directory_uri() . "/assets/images/filtres.png"; ?>"> Filtres
                </button>
                <img id="preload" style="display:none;width: 55px;float: right;"
                     src="<?php echo get_template_directory_uri() . "/assets/images/_preload.gif"; ?>">
                <div class="listSector" style="clear: both;display: none">
                    <?php
                    foreach ($sectors as $eachSector) {
                        ?>
                        <label for="<?php echo $eachSector->id; ?>" class="small-6 columns searchSectors">
                            <input type="checkbox" name="searchSectors" id="<?php echo $eachSector->id; ?>"
                                   value="<?php echo $eachSector->name; ?>">
                            <span><?php echo $eachSector->name; ?></span>
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id='searchContainer' style="position: fixed;"></div>
