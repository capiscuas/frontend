<?php
function getSectors() {
	global $baseApiInternalUrl;
	$request = $baseApiInternalUrl . "/mainSectors";
	$response = wp_remote_get($request);
	$json_response = json_decode(wp_remote_retrieve_body($response));
	return $json_response;
}

$sectors = getSectors();

$queryPost = array(
        'post_type'=> 'post',
        'post_status'=> 'publish',
        'posts_per_page'=> '2');
?>

<script>
<?php
echo 'var jsSiteUrl = \'' . get_site_url() . '\'; ';
echo 'var jsBaseResourceUrl = \'' . $baseApiUrl . '\';';
if (isset($mapEntityStatuses)) {
	echo "var mapEntityStatuses = ['" . implode("','", $mapEntityStatuses) . "']; ";
} else {
	echo 'var mapEntityStatuses = []; ';
}
if (isset($mapFilterTags)) {
	echo "var mapFilterTags = ['" . implode("','", $mapFilterTags) . "']; ";
} else {
	echo 'var mapFilterTags = []; ';
}
if (isset($_GET['center'])) {
	echo 'var mapCenter = ' . $_GET['center'] . '; ';
} else {
	echo 'var mapCenter = [41.58, 1.24]; ';
}
if (isset($_GET['zoom'])) {
	echo 'var mapZoom = ' . $_GET['zoom'] . '; ';
} else {
	echo 'var mapZoom = 7.7; ';
}
?>
</script>