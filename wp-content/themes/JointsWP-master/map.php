<?php
$base_dir = dirname(dirname(__FILE__));
require_once($base_dir."../../../wp-load.php");

function getEmbeddedMapConfig($domain) {
	global $baseApiInternalUrl;
	$encodedDomain = str_replace(".", "_", $domain);
	$request = $baseApiInternalUrl . "/embeddedMapConfig/" . $encodedDomain;
	$response = wp_remote_get($request);
	$json_response = json_decode(wp_remote_retrieve_body($response));
	return $json_response;
}
 
$refererDomain = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
$localDomain = parse_url(get_site_url(), PHP_URL_HOST);

if ($refererDomain == $localDomain) {
	$pamapamSite = true;
	$mapEntityStatuses = array('PUBLISHED');
	$mapFilterTags = array();
	include 'embeddedMap.php';
} else {
	$pamapamSite = false;
	$embeddedMapConfig = getEmbeddedMapConfig($refererDomain);
	
	if ($embeddedMapConfig->status == 200) {
		$requestApiKey = $_GET['apiKey'];
		
		if ($embeddedMapConfig->response->apiKey == $requestApiKey) {
			$mapEntityStatuses = array();
			foreach ($embeddedMapConfig->response->statuses as $eachStatus) {
				array_push($mapEntityStatuses, $eachStatus->entityStatusType);
			}
			$mapFilterTags = array();
			foreach ($embeddedMapConfig->response->externalFilterTags as $eachFilterTag) {
				array_push($mapFilterTags, $eachFilterTag->tagText);
			}
			include 'embeddedMap.php';
		}
	}
}

?>