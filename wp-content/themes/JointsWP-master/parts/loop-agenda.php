	<div class="box-celitos">

		<div class="bloc-xinxeta">
			<div class="celito left green"></div>
			<div class="celito right orange"></div>
			<div class="large-4 medium-4 small-12">
				<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a>
			</div>
			<div id="main" class="large-8 medium-8 small-12 single-xinxeta" role="main">
					<?php
						$categories = get_the_terms($post->ID, 'tipus'); //obting el tipus d'activitats
						if ($categories) { // si hi ha tipus d'activitats
							$llista_categories = array(); // creo una variable per guardar la llista
							foreach ($categories as $categoria) {
								array_push($llista_categories, $categoria->name);
							}
						$llista = implode($llista_categories, ' - ');
						echo "<span class='llista_categories'>$llista</span>"; // i la mostro
						}
					?>
				<h1><?php the_title(); ?></h1>
				<div class="large-8 medium-8 small-8">
					<div id="dades">
						<ul class="dades_agenda">
						<li class="lloc">Lloc: <?php echo get_post_meta($post->ID, 'lloc', true); ?></li>
						<?php
						$data_agenda= get_post_meta($post->ID, 'data', true);
						$dia_agenda = substr($data_agenda,6,2);
						$mes_agenda = substr($data_agenda,4,2);
						$any_agenda = substr($data_agenda,0,4);
						?>
						<li class="data">Data: <?php echo $dia_agenda; ?>/<?php echo $mes_agenda; ?>/<?php echo $any_agenda; ?></li>

						<li class="hora">Hora d'inici: <?php echo get_post_meta($post->ID, 'hora_inici', true); ?></li>
						<li class="hora">Hora de finalització: <?php echo get_post_meta($post->ID, 'hora_fi', true); ?></li>
						<li class='adrecat'>Adreçat a: <?php echo get_post_meta($post->ID, 'adrecat_a', true); ?></li>
					</ul>
					</div>
					<section class="entry-content" itemprop="articleBody">
				        <?php  the_content() ?>
					</section>
					<?php $inscriute = get_post_meta($post->ID, 'inscripcio', true); ?>
					<?php if (!empty($inscriute)) { ?>
						<p><a class="button" href="<?php echo $inscriute; ?>"> Inscriu-te</a></p>
					<?php } ?>
				</div>
		</div>


	</div>








	</article> <!-- end article -->
	</div>
