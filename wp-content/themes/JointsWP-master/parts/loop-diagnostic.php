	<div class="box-celitos">

		<div class="bloc-xinxeta">
			<div class="celito left green"></div>
			<div class="celito right orange"></div>
			<div class="large-4 medium-4 small-12">
				<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a>
			</div>
			<div id="main" class="large-8 medium-8 small-12 single-xinxeta" role="main">
					<?php
						$categories = get_the_terms($post->ID, 'tipus'); //obting el tipus d'activitats
						if ($categories) { // si hi ha tipus d'activitats
							$llista_categories = array(); // creo una variable per guardar la llista
							foreach ($categories as $categoria) {
								array_push($llista_categories, $categoria->name);
							}
						$llista = implode($llista_categories, ' - ');
						echo $llista; // i la mostro
						}
					?>
				<h1><?php the_title(); ?></h1>
		 		<div class="large-8 medium-8 small-8">
					<div id="dades">
						<ul class="dades_agenda">
							<?php
								$data_diagnostic= get_post_meta($post->ID, 'data', true);
								$dia_diagnostic = substr($data_diagnostic,6,2);
								$mes_diagnostic = substr($data_diagnostic,4,2);
								$any_diagnostic = substr($data_diagnostic,0,4);
							?>
							<li class="any-diagnostic">Data publicació: <?php echo $dia_diagnostic; ?>/<?php echo $mes_diagnostic; ?>/<?php echo $any_diagnostic; ?></li>
							<li class="sector-diagnostic">Sector: <?php echo get_post_meta($post->ID, 'sector', true); ?></li>
							<li class="territori-diagnostic">Territori: <?php echo get_post_meta($post->ID, 'territori', true); ?></li>
						</ul>
					</div>
					<section class="entry-content" itemprop="articleBody">
				        <?php  the_content() ?>
					</section>
					<?php $linkPdf = get_post_meta($post->ID, 'link_pdf', true); ?>
					<?php if (!empty($linkPdf)) { ?>
						<p><a class="button" href="<?php echo $linkPdf; ?>"> Descarrega</a></p>
					<?php } ?>
				</div>
			</div>
        </div>








	</article> <!-- end article -->
		</div>
