<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->

<div class="top-bar" id="top-bar-menu">
	<a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pamapam-logo.png" alt="Logo Pam a Pam" /></a>
	<div class="menu-centered show-for-medium">
		<?php joints_top_nav(); ?>
	</div>
	<div class="top-bar-right float-right show-for-small-only">
		<ul class="menu">
			<li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
			<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
		</ul>
	</div>
    <?php if(is_home()) { // activa solo si estas en la home?>
        <div>
            <span class="search-menu show-for-small-only">
                <img  src="<?php echo get_template_directory_uri() ."/assets/images/fi-magnifying-glass-white.svg";?>">
            </span>
        </div>
    <?php }?>

</div>
