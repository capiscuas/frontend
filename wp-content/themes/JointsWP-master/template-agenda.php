<?php /* Template Name: AGENDA */ ?>

<!-- inici menú -->

<?php get_header(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/activitats-landing.js"></script>
<div class="row upper-mar">
<div class="box-head">
  <h1 class="page-title"><?php the_title(); ?></h1>
  <?php echo addVesAlMapa(); ?>
</div>
</div>

<!-- fi menú-->


<!-- Loop de consulta a la base de dades  marga-- >

<?php

 $today = current_time('Y-m-d');

 $queryPost = array( 'post_type'=> 'Agenda',
  'post_status'=> 'publish',
  'meta_key'			=> 'data',
  // només les activitats futures
  'meta_query'             => array(
      array(
          'key'       => 'data',
          'value'     => $today,
          'compare'   => '>=',
          'type'      => 'DATE',
      ),
  ),
  'orderby'			=> 'meta_value',
	'order'				=> 'ASC'
     //'posts_per_page'=> '10',
     //'paged' => get_query_var('paged')
   )
?>

<!-- fin loop de consulta a la base de dades  marga-- >




<!-- inici buscador ona -->

  <!-- disseny: sense celos -->
  <!--
  <?php
  $terms = get_terms( array(
    'taxonomy' => 'tipus',
    'hide_empty' => false,
  ) );
  ?>

  <form id="searchTipusForm">
    <input type="submit" style="display: none" />
    <div class="row">
     <div class="large-4 columns">
      <select id="searchTipus">

         <?php
          foreach($terms as $tipus):
            echo '<option id="'.$tipus->name.'" value="'.$tipus->name.'">'.$tipus->name.'</option>'; //close your tags!!
          endforeach;
        ?>
      </select>
     </div>
   </div>
  </form>
  -->
  <!-- fi buscador -->



<!-- inici llista -->


<div id="content">
	<div id="inner-content" class="row"> <!-- aquesta div fa marges al contingut. potser s'haurà de posar dins del filtre(?) -->

<?php query_posts($queryPost);?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <?php get_template_part( 'parts/loop', 'agenda' ); ?>
<?php endwhile; ?>
<?php endif; wp_reset_query(); ?>

  </div>
</div>

<!-- fi llista -->


<!-- inici xarxes socials eric-->

<!-- banners afiliacions socials -->
  <div class="social-afiliations">

    <div class="row">
      <div class="large-6 small-12 columns">

        <div class="large-12 columns in">
          <div class="celito purple"></div>

          <p class="large-7 small-12 columns">segueix-nos</p>
          <ul class="large-5 small-12 columns">
            <li><a href="https://www.facebook.com/pamapamCAT/" target="_blank"><span class="icon-facebook"></span></a></li>
            <li><a href="https://twitter.com/pamapamcat?lang=es" target="_blank"><span class="icon-twitter"></span></a></li>
            <li><a href="https://www.instagram.com/pamapamcat/" target="_blank"><span class="icon-instagram"></span></a></li>
          </ul>
        </div>
      </div>
      <div class="large-6 small-12 columns">


        <div class="large-12 columns in">
          <div class="celito green"></div>

          <p class="large-7 small-12 columns">butlletí de notícies</p>
          <p class="large-5 small-12 columns"><a href="/butlleti/" class="">Apunta-t'hi <span class="icon-arrow-right"></span></a></p>
      </div>
      </div>
   </div>

 </div>
  <!-- end banners afiliacions socials -->

<!-- fi xarxes socials -->



<!-- inici footer eric-->

<?php get_footer(); ?>

<!-- fi footer -->
