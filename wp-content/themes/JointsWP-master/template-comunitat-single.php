<?php /* Template Name: SINGLE Comunitat */ ?>

<?php
$baseApiInternalUrl;
$baseApiUrl;

$url = $_SERVER["REQUEST_URI"];
$userId = explode('/',$url);
$userId = $userId[4];

$entityRequest = $baseApiInternalUrl . "/users/" . $userId;
$entityResponse = wp_remote_get($entityRequest);
$pamapamUser = json_decode(wp_remote_retrieve_body($entityResponse))->response;
?>

<?php get_header(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/comunitat-single.js"></script>
<div id="content">
	<div id="inner-content" class="row">
		<div class="box-head">
			<h1 class="page-title"><?php the_title(); ?></h1>
			<span class=link-to-map><a href="<?php echo get_home_url(); ?>" class="button">VES AL MAPA</a></span>
			<p><?php the_content(); ?></p>
		</div>
		<?php user_single($pamapamUser); ?>
		<br/>

		<!-- Published entities -->
		<div class="row related">
			<div class="small-12 medium-12 large-12 columns padding-20" data-equalizer-watch="">
				<p>Punts publicats</p>
				<hr/>
				<div id="punts" class="container">
<?php
					published_entities($userId);
?>
		    	</div>
		    </div>
		</div>

		<!-- Published articles -->
		<div class="small-12 medium-12 large-12 columns padding-20" data-equalizer-watch="">
			<p>Contingut publicat</p>
			<hr/>
<?php
			$currentUserPosts = [];
			$wpUser = get_user_by('login', $pamapamUser->username);
			if ($wpUser) {
				$args = array(
						'author' => $wpUser->ID,
						'orderby' => 'post_date',
						'order' => 'ASC',
						'posts_per_page' => -1
				);

				$currentUserPosts = get_posts($args);
			}

			foreach ($currentUserPosts as $post){
				echo "<p><a href='$post->guid'>$post->post_title</a></p>";
				$fecha_entero = strtotime($post->post_date);
				$fecha_formato = date ("j/n/Y", $fecha_entera);
				echo "<p>$fecha_formato</p>";
				$contingut = strip_tags($post->post_content);
				$contigut_retallat = substr($contingut,0,strpos($contingut, ' ',120));
				echo "<p>$contigut_retallat...</p>";
			}
?>
		</div>
	</div>
</div>

<div class="contain-to-grid sand-bkg">
	<?php get_template_part('parts/include', 'afiliation'); ?>
</div>

<?php get_footer(); ?>