<?php /* Template Name: ETS-INICIATIVA */ ?>


<?php

get_header();
?>
<script src="<?php echo get_template_directory_uri(); ?>/js/ets-iniciativa.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.min.js"></script>

<script>
<?php
echo 'var jsSiteUrl = \'' . get_site_url() . '\'; ';
echo 'var jsBaseApiUrl = \'' . $baseApiUrl . '\'; ';
?>
</script>

<div class="row">
	<div class="large-6 small-12 columns">
		<span class=link-to-inici><a href="<?php get_site_url(); ?>" class="button button-large"><i class="icon-arrow-left2 margin-right-5"></i>Inici</a></span>
		<div class="row">
			<div class="large-8 small-8 large-centered small-centered columns">
				<img src="<?php echo get_template_directory_uri()?>/assets/images/pamapam-logo-bn.png" width="100%">
			</div>
			<div class="small-12 small-centered columns margin-top-15 padding-20">
				<h5><?php
                                if (get_field('texto-iniciativa')) {
                                    echo get_field('texto-iniciativa');
                                }
                            ?>
				</h5>
			</div>
		</div>
		<div class="row">
			<div class="large-10 small-8 large-centered small-centered columns">
				<form action= get_site_url() . "ok-ets-iniciativa/" . "add&noheader=true" class="xinxeta-form" method="post">
					<!--DADES BASISQUES-->
					<div class="row">
						<div class="large-12 columns">
							<p class="border-left-celito">DADES BÀSIQUES</p>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Nom de la iniciativa</p>
							<input id="f_name" type="text" name="f_name" class="input" required>
						</div>
						<div class="large-6 columns">
							<p class="form-label">NIF/CIF</p>
							<input id="f_nif" type="text" name="f_nif" class="input">
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Província</p>
							<select id="f_province" name="f_province">
							</select>
						</div>
						<div class="large-6 columns">
							<p class="form-label">Comarca</p>
							<select id="f_region" name="f_region">
							</select>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Població</p>
							<select id="f_town" name="f_town">
							</select>
						</div>
						<div class="large-6 columns">
							<p class="form-label">Districte</p>
							<select id="f_district" name="f_district">
							</select>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Barri</p>
							<select id="f_neighborhood" name="f_neighborhood">
							</select>
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns">
							<p class="form-label">Horari</p>
							<input id="f_openingHours" type="text" name="f_openingHours">
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns">
							<p class="form-label">Què hi pots trobar?</p>
							<input type="text" id="f_productTags" name="f_productTags" placeholder="<?php esc_attr_e ('Etiqueta1 Etiqueta2 Etiqueta3 (sense commas)');?>">
						</div>
					</div>
					<!--ADREÇA-->
					<div class="row">
						<div class="large-12 columns">
							<p class="border-left-celito">ADREÇA</p>
						</div>
					</div>
					<div class="row">
						<div class="large-8 columns">
							<p class="form-label">Avinguda, carrer, plaça...</p>
							<input id="f_address" type="text" name="f_address" class="input" placeholder="<?php esc_attr_e ('Adreça');?>" required>
						</div>
						<div class="large-4 columns">
							<p class="form-label">Número</p>
							<input id="f_number" type="text" name="f_number" class="input" required>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Correu elèctronic de contacte</p>
							<input id="f_email" type="mail" name="f_email" class="input" required pattern="^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$">
						</div>
						<div class="large-6 columns">
							<p class="form-label">Telèfon de contacte</p>
							<input id="f_phone" type="tel" name="f_phone">
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Lloc web</p>
							<input id="f_web" type="url" name="f_web">
						</div>
						<div class="large-6 columns">
							<p class="form-label">Twitter</p>
							<input id="f_twitter" type="url" name="f_twitter">
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Facebook</p>
							<input id="f_facebook" type="url" name="f_facebook">
						</div>
						<div class="large-6 columns">
							<p class="form-label">G+</p>
							<input id="f_googlePlus" type="url" name="f_googlePlus">
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Instagram</p>
							<input id="f_instagram" type="url" name="f_instagram">
						</div>
						<div class="large-6 columns">
							<p class="form-label">Pinterest</p>
							<input id="f_pinterest" type="url" name="f_pinterest">
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Quitter</p>
							<input name="f_quitter" type="url" name="f_quitter">
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns">
							<p class="form-label">Breu descripció de la iniciativa</p>
                            <textarea id="f_description" name="f_description" cols="40" rows="5" class="descripcio"></textarea>
						</div>
						</div>
					<!--AMBIT DE L'ECONOMIA SOCIAL I SOLIDARIA-->
					<div class="row">
						<div class="large-12 columns">
							<p class="border-left-celito">ÀMBIT DE L'ECONOMIA SOCIAL I SOLIDÀRIA</p>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Sectors</p>
							<select id="f_sector" name="f_sector">
							</select>
						</div>
						<div class="large-6 columns">
							<p class="form-label">Forma jurídica</p>
							<select id="f_legalForm" name="f_legalForm">
							</select>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Xarxes a les que es pertany</p>
							<select id="f_socialEconomyNetworks" name="f_socialEconomyNetworks[]" multiple>
							</select>
						</div>
						<div class="large-6 columns">
							<p class="form-label">Iniciatives amb les que es col.labora</p>
							<textarea id="f_collaboratingWith" name="f_collaboratingWith" cols="40" rows="5" class="descripcio"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns">
							<p class="form-label">Balanç social</p>
							<div class="radioButton">
								<input type="radio" name="socialBalance" value="1"><label>Sí</label>
								<input type="radio" name="socialBalance" value="2" checked><label>No</label>
								<input type="radio" name="socialBalance" value="3"><label>No aplica</label>
							</div>
						</div>
					</div>
					<div class="small-8 small-centered columns">
						<button type="submit" name="iniciativa-submit" class="button-submit" value="">
							<span>Proposa una iniciativa</span> <i class="icon icon-arrow-right "></i>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

   <div class="large-6 columns small-12 voluntaria-background" style="background-image: url('<?php echo get_template_directory_uri()?>/assets/images/09_iniciativa-gran.JPG');">
    </div>

</div>
<script type="text/javascript">
	$(document).foundation();
</script>

