<?php /* Template Name: Organizació */ ?>

<?php get_header(); ?>
<div class="row upper-mar">
<div class="box-head">
  <h1 class="page-title"><?php the_title(); ?></h1>
  <?php echo addVesAlMapa(); ?>
</div>
</div>

<div class="ima-text"><img src="<?php echo get_field("capçalera"); ?>"/>

<div class="txt-encima-ima">

<div class="row">
  <div class="large-6 medium-6 small-12 columns txt-destacado"><?php the_field('text_esquerra_foto_org'); ?></div>
  <div class="large-6 medium-6 small-12 columns parrafo-foto"><?php the_field('text_dreta_foto_org'); ?></div>
</div>
</div>

</div>
<div id="content">

		<div id="inner-content" class="row">





<div class="large-12 medium-12 small-12 columns back-grey">
<div class="large-6 medium-6 small-12 columns">
  <h4 class="titol-espais"><?php the_field('titol_espais_participacio'); ?></h4>
  <img src="<?php echo get_field("foto_cos"); ?>"/>

</div>
<div class="large-6 medium-6 small-12 columns">
<!--loop espais_participació-->
<?php $query = new WP_Query( array(
                  'post_type' => array('espais_participacio',),

                  'posts_per_page' => -1

              ) );
              while ($query->have_posts()) : $query->the_post(); ?>


<article id="post-<?php the_ID(); ?>" class="large-12 medium-12 small-12 columns espais-block" role="article" itemscope itemtype="http://schema.org/BlogPosting">

<h5><?php the_title(); ?></h5>

<p>
      <?php the_excerpt(); ?>
      </p>
<br/>


</article>


      <?php
    endwhile;
    wp_reset_postdata();
    ?>
<!--fin del loop de espais_participació-->

</div>


    </div>



    <div class="large-12 medium-12 small-12 columns back-grey-2">
      <p><?php the_field('text_espais_participacio'); ?></p>
    </div>

  <a href="<?php echo get_home_url(); ?>/xinxeta/">
    <div class="large-12 medium-12 small-12 columns">
    <div class="xinxeta-box large-4 large-offset-4 medium-8 medium-offset-2 small-12 columns">
<span class="xin">Xinxeta</span>
      <p><?php the_field('fes_te_xinxeta'); ?></p>
    </div>
  </div>
</a>









  </div>

</div>

<?php get_footer(); ?>
