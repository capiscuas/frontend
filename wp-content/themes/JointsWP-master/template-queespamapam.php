<?php
/*
Template Name: Què és PAMAPAM (No Sidebar)
*/
?>
<?php get_header(); ?>
	<div id="content">
		<div id="inner-content" class="row">
		    <main id="main" class="large-12 medium-12 columns" role="main">
                <div class="box-head">
                    <h1 class="page-title"><?php the_title(); ?></h1>
                    <?php echo addVesAlMapa(); ?>
                </div>
                <div class="large-12 small-12 columns centered menu-static">
                    <?php joints_static_nav(); ?>
                </div>
                <hr>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php get_template_part( 'parts/loop', 'page' ); ?>
				<?php endwhile; endif; ?>
			</main> <!-- end #main -->
		</div> <!-- end #inner-content -->
	</div> <!-- end #content -->
<?php get_footer(); ?>
