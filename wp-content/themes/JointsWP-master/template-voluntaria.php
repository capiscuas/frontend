<?php /* Template Name: FES-TE VOLUNTARIA */ ?>


<?php
get_header(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/voluntaria.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.min.js"></script>

<script>
<?php
echo 'var jsSiteUrl = \'' . get_site_url() . '\'; ';
echo 'var jsBaseApiUrl = \'' . $baseApiUrl . '\'; ';
?>
</script>

<div class="row">
    <div class="large-6 small-12 columns">
        <span class=link-to-inici><a href="<?php echo get_site_url();?>" class="button button-large"><i class="icon-arrow-left2 margin-right-5" ></i>Inici</a></span>
        <div class="row">
            <div class="large-8 small-8 large-centered small-centered columns">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/pamapam-logo-bn.png" width="100%">
            </div>
            <div class="small-12 small-centered columns margin-top-15">
                <h4><?php
                                if (get_field('texto-proposta')) {
                                    echo get_field('texto-proposta');
                                }
                            ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="large-10 small-8 large-centered small-centered columns">
                <form action="" class="xinxeta-form" method="POST">
                    <!--DADES BASISQUES-->
                    <div class="row">
                        <div class="large-12 columns">
                            <p class="border-left-celito">DADES BÀSIQUES</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 columns">
                            <p class="form-label">Nom de la iniciativa</p>
                            <input id="f_name" type="text" name="f_name" class="input" required>
                        </div>
                        <div class="large-6 columns">
                            <p class="form-label">Província</p>
                            <select id="f_provinces" name="f_province">
                            </select>
                        </div>
                    </div>
                    <div class="row">
                    <div class="large-6 columns">
                            <p class="form-label">Comarca</p>
                            <select id="f_regions" name="f_region">
                            </select>
                    </div>

                    <div class="large-6 columns">
                            <p class="form-label">Població</p>
                            <select id="f_towns" name="f_town">
                            </select>
                    </div>
                    </div>

                    <div class="row">

                       <div class="large-6 columns">
                            <p class="form-label">Districte</p>
                            <select id="f_districts" name="f_district">
                            </select>
	                   </div>

                       <div class="large-6 columns">
                            <p class="form-label">Barri</p>
                            <select id="f_neighborhoods" name="f_neighborhood">
                            </select>
                       </div>
                    </div>
                    <div class="row">
                        <div class="large-8 columns">
                            <p class="form-label">Avinguda, carrer, plaça...</p>
                            <input id="f_address" type="text" name="f_address" class="input" required>
                        </div>
                        <div class="large-4 columns">
                            <p class="form-label">Número</p>
                            <input id="f_number" type="text" name="f_number" class="input" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <p class="form-label">Breu descripció de la iniciativa</p>
                            <textarea id="f_description" name="f_description" cols="40" rows="5" class="descripcio"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 columns">
                            <p class="form-label">El teu nom</p>
                            <input id="f_proposerName" type="text" name="f_proposerName" class="input" required>
                        </div>
                        <div class="large-6 columns">
                            <p class="form-label">El teu email</p>
                            <input id="f_proposerEmail" type="mail" name="f_proposerEmail" class="input" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}">
                        </div>
                    </div>
                        <div class="row">
                        <div class="large-12 columns">
                            <p class="form-label">Comentaris</p>
                            <textarea id="f_comments" name="f_comments" cols="40" rows="5" class="comentaris"></textarea>
                        </div>
                    </div>
                    <div class="row">
                    <div class="large-6 columns">
                        <button type="submit" name="voluntaria-submit" class="button-submit" value="">
                            <span>Proposa una iniciativa</span> <i class="icon icon-arrow-right "></i>
                        </button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="large-6 columns small-12 voluntaria-background" style="background-image: url('<?php echo get_template_directory_uri()?>/assets/images/08_proposa_iniciativa-gran.JPG');">
    </div>

</div>
<script type="text/javascript">
    $(document).foundation();
</script>

